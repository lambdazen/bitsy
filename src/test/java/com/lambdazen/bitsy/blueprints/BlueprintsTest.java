package com.lambdazen.bitsy.blueprints;

import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import junit.framework.Test;

import com.lambdazen.bitsy.BitsyGraph;
import com.tinkerpop.blueprints.EdgeTestSuite;
import com.tinkerpop.blueprints.Graph;
import com.tinkerpop.blueprints.GraphQueryTestSuite;
import com.tinkerpop.blueprints.GraphTestSuite;
import com.tinkerpop.blueprints.KeyIndexableGraphTestSuite;
import com.tinkerpop.blueprints.TestSuite;
import com.tinkerpop.blueprints.ThreadedTransactionalGraphTestSuite;
import com.tinkerpop.blueprints.TransactionalGraphTestSuite;
import com.tinkerpop.blueprints.VertexQueryTestSuite;
import com.tinkerpop.blueprints.VertexTestSuite;
import com.tinkerpop.blueprints.impls.GraphTest;
import com.tinkerpop.blueprints.util.io.gml.GMLReaderTestSuite;
import com.tinkerpop.blueprints.util.io.graphml.GraphMLReaderTestSuite;
import com.tinkerpop.blueprints.util.io.graphson.GraphSONReaderTestSuite;

public class BlueprintsTest extends GraphTest implements Test {
    public BlueprintsTest() {
    
    }
    
    public void setUp() {
    }
    
    @Override
    public Graph generateGraph() {
        return generateGraph("bitsy");
    }
    
    @Override
    public Graph generateGraph(String graphDirectoryName) {
        Path dbPath = Paths.get(computeTestDataRoot() + "/" + graphDirectoryName + "/");
        if (!Files.exists(dbPath)) {
            try {
                if (!Files.exists(computeTestDataRoot().toPath())) {
                    Files.createDirectory(computeTestDataRoot().toPath());
                }
                Files.createDirectory(dbPath);
            } catch (IOException e) {
                e.printStackTrace();
            }            
        }
        
        return new BitsyGraph(dbPath);
    }

    public void doTestSuite(final TestSuite testSuite) throws Exception {
        deleteDirectory(this.computeTestDataRoot());
        for (Method method : testSuite.getClass().getDeclaredMethods()) {
            if (method.getName().startsWith("test")) {
                System.out.println("Testing " + method.getName() + "...");
                method.invoke(testSuite);
                deleteDirectory(this.computeTestDataRoot());
            }
        }
    }

    public void testVertexTestSuite() throws Exception {
        this.stopWatch();
        doTestSuite(new VertexTestSuite(this));
        printTestPerformance("VertexTestSuite", this.stopWatch());
    }

    public void testEdgeTestSuite() throws Exception {
        this.stopWatch();
        doTestSuite(new EdgeTestSuite(this));
        printTestPerformance("EdgeTestSuite", this.stopWatch());
    }

    public void testGraphTestSuite() throws Exception {
        this.stopWatch();
        doTestSuite(new GraphTestSuite(this));
        printTestPerformance("GraphTestSuite", this.stopWatch());
    }

    public void testQueryTestSuite() throws Exception {
        this.stopWatch();
        doTestSuite(new VertexQueryTestSuite(this));
        doTestSuite(new GraphQueryTestSuite(this));
        printTestPerformance("QueryTestSuite", this.stopWatch());
    }

    public void testKeyIndexableGraphTestSuite() throws Exception {
        this.stopWatch();
        doTestSuite(new KeyIndexableGraphTestSuite(this));
        printTestPerformance("KeyIndexableGraphTestSuite", this.stopWatch());
    }

    // TODO: Re-enable after finding out the cobertura + maven surefire incompatibility
    public void untestGraphMLReaderTestSuite() throws Exception {
        this.stopWatch();
        doTestSuite(new GraphMLReaderTestSuite(this));
        printTestPerformance("GraphMLReaderTestSuite", this.stopWatch());
    }

    // TODO: Re-enable after finding out the cobertura + maven surefire incompatibility
    public void untestGraphSONReaderTestSuite() throws Exception {
        this.stopWatch();
        doTestSuite(new GraphSONReaderTestSuite(this));
        printTestPerformance("GraphSONReaderTestSuite", this.stopWatch());
    }

    // TODO: Re-enable after finding out the cobertura + maven surefire incompatibility
    public void untestGMLReaderTestSuite() throws Exception {
        this.stopWatch();
        doTestSuite(new GMLReaderTestSuite(this));
        printTestPerformance("GMLReaderTestSuite", this.stopWatch());
    }

    public void testTransactionalGraphTestSuite() throws Exception {
        this.stopWatch();
        doTestSuite(new TransactionalGraphTestSuite(this));
        printTestPerformance("TransactionalGraphTestSuite", this.stopWatch());
    }

    public void testThreadedTransactionalGraphTestSuite() throws Exception {
        this.stopWatch();
        doTestSuite(new ThreadedTransactionalGraphTestSuite(this));
        printTestPerformance("ThreadedTransactionalGraphTestSuite", this.stopWatch());
    }
    
//        public Graph generateGraph() {
//            return generateGraph(false);
//        }
//
//        public Graph generateGraph(boolean create) {
//            return this.generateGraph(create, "blueprints_test.dex");
//        }
//
//        public Graph generateGraph(final String graphDirectoryName) {
//            return this.generateGraph(false, graphDirectoryName);
//        }
//
//        public Graph generateGraph(boolean create, final String graphDirectoryName) {
//            String db = this.computeTestDataRoot() + "/" + graphDirectoryName;
//
//            if (create) {
//                deleteDirectory(this.computeTestDataRoot());
//            }
//
//            return new DexGraph(db, "./blueprints-dex.cfg");
//        }
//    }
}
