package com.lambdazen.bitsy;

import com.lambdazen.bitsy.store.Record;
import com.lambdazen.bitsy.store.Record.RecordType;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.TransactionalGraph.Conclusion;

public class BitsyMemGraphTest extends BitsyGraphTest {
    public boolean isPersistent() {
        return false;
    }
    
    public void setUp() {
        System.out.println("Setting up memory-only graph");
        graph = new BitsyGraph(false);
    }
    
    public void tearDown() {
        System.out.println("Tearing down graph");
        graph.shutdown();
    }

    public void testPersistence() {
        // Disable
    }
    
    public void testObsolescence() {
        IGraphStore store = ((BitsyGraph)graph).getStore();
        
        // Create a vertex
        Vertex v = graph.addVertex(null);
        Object vid = v.getId();
        v.setProperty("foo", "bar");
        
        // Self edge
        Edge e = graph.addEdge(null, v, v, "self");
        Object eid = e.getId();
        
        graph.stopTransaction(Conclusion.SUCCESS);

        Record v1MRec = new Record(RecordType.V, "{\"id\":\"" + vid + "\",\"v\":1,\"s\":\"M\"}");
        assertFalse(v1MRec.checkObsolete(store, false, 1, null));
        assertFalse(v1MRec.checkObsolete(store, true, 1, null));

        Record e1MRec = new Record(RecordType.E, "{\"id\":\"" + eid + "\",\"v\":1,\"s\":\"M\",\"o\":\"" + vid + "\",\"l\":\"" + vid + "\",\"i\":\"" + vid + "\"}");
        assertFalse(e1MRec.checkObsolete(store, false, 1, null));
        assertFalse(e1MRec.checkObsolete(store, true, 1, null));

        // Create a vertex
        v = graph.getVertex(vid);
        v.setProperty("foo", "baz");

        e = graph.getEdge(eid);
        e.setProperty("foo", "baz");

        graph.stopTransaction(Conclusion.SUCCESS);

        Record v2MRec = new Record(RecordType.V, "{\"id\":\"" + vid + "\",\"v\":2,\"s\":\"M\"}");
        Record v1DRec = new Record(RecordType.V, "{\"id\":\"" + vid + "\",\"v\":1,\"s\":\"D\"}");
        
        assertTrue(v1MRec.checkObsolete(store, false, 1, null));
        assertTrue(v1MRec.checkObsolete(store, true, 1, null));

        assertFalse(v1DRec.checkObsolete(store, false, 1, null));
        assertTrue(v1DRec.checkObsolete(store, true, 1, null));

        assertFalse(v2MRec.checkObsolete(store, false, 1, null));
        assertFalse(v2MRec.checkObsolete(store, true, 1, null));

        Record e2MRec = new Record(RecordType.E, "{\"id\":\"" + eid + "\",\"v\":2,\"s\":\"M\",\"o\":\"" + vid + "\",\"l\":\"" + vid + "\",\"i\":\"" + vid + "\"}");
        Record e1DRec = new Record(RecordType.E, "{\"id\":\"" + eid + "\",\"v\":1,\"s\":\"D\",\"o\":\"" + vid + "\",\"l\":\"" + vid + "\",\"i\":\"" + vid + "\"}");
        
        assertTrue(e1MRec.checkObsolete(store, false, 1, null));
        assertTrue(e1MRec.checkObsolete(store, true, 1, null));

        assertFalse(e1DRec.checkObsolete(store, false, 1, null));
        assertTrue(e1DRec.checkObsolete(store, true, 1, null));

        assertFalse(e2MRec.checkObsolete(store, false, 1, null));
        assertFalse(e2MRec.checkObsolete(store, true, 1, null));

        // Delete vertex
        v = graph.getVertex(vid);
        graph.removeVertex(v);

        // Edge will get deleted automatically!
        
        graph.stopTransaction(Conclusion.SUCCESS);
        
        Record v2DRec = new Record(RecordType.V, "{\"id\":\"" + vid + "\",\"v\":1,\"s\":\"D\"}");
        assertFalse(v2DRec.checkObsolete(store, false, 1, null));
        assertTrue(v2DRec.checkObsolete(store, true, 1, null));
    }

}
