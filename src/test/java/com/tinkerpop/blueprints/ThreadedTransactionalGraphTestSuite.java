package com.tinkerpop.blueprints;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import com.tinkerpop.blueprints.TransactionalGraph.Conclusion;
import com.tinkerpop.blueprints.impls.GraphTest;

/**
 * @author Marko A. Rodriguez] (http://markorodriguez.com)
 */
public class ThreadedTransactionalGraphTestSuite extends TestSuite {
    public ThreadedTransactionalGraphTestSuite() {
    }

    public ThreadedTransactionalGraphTestSuite(final GraphTest graphTest) {
        super(graphTest);
    }

    public void testTrue() {
        assertTrue(true);
    }

    public void testRepeatedTransactionStopException() {
        ThreadedTransactionalGraph graph = (ThreadedTransactionalGraph) graphTest.generateGraph();
        
        if (!graph.getFeatures().supportsThreadedTransactions) {
            return;
        }

        TransactionalGraph txGraph = graph.newTransaction();
        
        txGraph.commit();
        txGraph.stopTransaction(Conclusion.FAILURE);
        txGraph.stopTransaction(Conclusion.SUCCESS);
        txGraph.shutdown();
        
        graph.shutdown();
    }

    public void testAutoStartTransaction() {
        ThreadedTransactionalGraph graph = (ThreadedTransactionalGraph) graphTest.generateGraph();

        if (!graph.getFeatures().supportsThreadedTransactions) {
            return;
        }
        
        TransactionalGraph txGraph = graph.newTransaction();
        vertexCount(txGraph,0);
        Vertex v1 = txGraph.addVertex(null);
        vertexCount(txGraph,1);
        assertEquals(v1.getId(), txGraph.getVertex(v1.getId()).getId());
        txGraph.stopTransaction(Conclusion.SUCCESS);
        vertexCount(txGraph,1);
        assertEquals(v1.getId(),txGraph.getVertex(v1.getId()).getId());
        txGraph.shutdown();
        
        graph.shutdown();
    }

    public void testTransactionsForVertices() {
        ThreadedTransactionalGraph graph = (ThreadedTransactionalGraph) graphTest.generateGraph();

        if (!graph.getFeatures().supportsThreadedTransactions) {
            return;
        }

        TransactionalGraph txGraph = graph.newTransaction();
        List<Vertex> vin = new ArrayList<Vertex>();
        List<Vertex> vout = new ArrayList<Vertex>();
        vin.add(txGraph.addVertex(null));
        txGraph.stopTransaction(Conclusion.SUCCESS);
        vertexCount(txGraph,1);
        containsVertices(txGraph,vin);
        
        this.stopWatch();
        vout.add(txGraph.addVertex(null));
        vertexCount(txGraph,2);
        containsVertices(txGraph,vin); containsVertices(txGraph,vout);
        txGraph.rollback();

        containsVertices(txGraph,vin);
        vertexCount(txGraph,1);
        printPerformance(txGraph.toString(), 1, "vertex not added in failed transaction", this.stopWatch());

        this.stopWatch();
        vin.add(txGraph.addVertex(null));
        vertexCount(txGraph,2);
        containsVertices(txGraph,vin);
        txGraph.commit();
        printPerformance(txGraph.toString(), 1, "vertex added in successful transaction", this.stopWatch());
        vertexCount(txGraph,2);
        containsVertices(txGraph,vin);

        txGraph.shutdown();
        
        graph.shutdown();
    }

    public void testBruteVertexTransactions() {
        ThreadedTransactionalGraph graph = (ThreadedTransactionalGraph) graphTest.generateGraph();

        if (!graph.getFeatures().supportsThreadedTransactions) {
            return;
        }
        
        TransactionalGraph txGraph = graph.newTransaction();
        List<Vertex> vin = new ArrayList<Vertex>(), vout = new ArrayList<Vertex>();
        this.stopWatch();
        for (int i = 0; i < 100; i++) {
            vin.add(txGraph.addVertex(null));
            txGraph.commit();
        }
        printPerformance(txGraph.toString(), 100, "vertices added in 100 successful transactions", this.stopWatch());
        vertexCount(txGraph,100);
        containsVertices(txGraph,vin);

        this.stopWatch();
        for (int i = 0; i < 100; i++) {
            vout.add(txGraph.addVertex(null));
            txGraph.rollback();
        }
        printPerformance(txGraph.toString(), 100, "vertices not added in 100 failed transactions", this.stopWatch());

        vertexCount(txGraph,100);
        containsVertices(txGraph,vin);
        txGraph.stopTransaction(Conclusion.FAILURE);
        vertexCount(txGraph,100);
        containsVertices(txGraph,vin);


        this.stopWatch();
        for (int i = 0; i < 100; i++) {
            vin.add(txGraph.addVertex(null));
        }
        vertexCount(txGraph,200);
        containsVertices(txGraph,vin);
        txGraph.stopTransaction(Conclusion.SUCCESS);
        printPerformance(txGraph.toString(), 100, "vertices added in 1 successful transactions", this.stopWatch());
        vertexCount(txGraph,200);
        containsVertices(txGraph,vin);

        this.stopWatch();
        for (int i = 0; i < 100; i++) {
            vout.add(txGraph.addVertex(null));
        }
        vertexCount(txGraph,300);
        containsVertices(txGraph,vin); containsVertices(txGraph,vout.subList(100,200));
        txGraph.rollback();
        printPerformance(txGraph.toString(), 100, "vertices not added in 1 failed transactions", this.stopWatch());
        vertexCount(txGraph,200);
        containsVertices(txGraph,vin);
        txGraph.shutdown();
        
        graph.shutdown();
    }

    public void testTransactionsForEdges() {
        ThreadedTransactionalGraph graph = (ThreadedTransactionalGraph) graphTest.generateGraph();
        
        if (!graph.getFeatures().supportsThreadedTransactions) {
            return;
        }

        TransactionalGraph txGraph = graph.newTransaction();

        Vertex v = txGraph.addVertex(null);
        Vertex u = txGraph.addVertex(null);
        txGraph.commit();

        this.stopWatch();
        Edge e = txGraph.addEdge(null, txGraph.getVertex(v.getId()), txGraph.getVertex(u.getId()), graphTest.convertLabel("test"));


        assertEquals(txGraph.getVertex(v.getId()), v);
        assertEquals(txGraph.getVertex(u.getId()), u);
        if (txGraph.getFeatures().supportsEdgeRetrieval)
            assertEquals(txGraph.getEdge(e.getId()), e);

        vertexCount(txGraph,2);
        edgeCount(txGraph,1);

        txGraph.stopTransaction(Conclusion.FAILURE);
        printPerformance(txGraph.toString(), 1, "edge not added in failed transaction (w/ iteration)", this.stopWatch());

        assertEquals(txGraph.getVertex(v.getId()), v);
        assertEquals(txGraph.getVertex(u.getId()), u);
        if (txGraph.getFeatures().supportsEdgeRetrieval)
            assertNull(txGraph.getEdge(e.getId()));

        if (txGraph.getFeatures().supportsVertexIteration)
            assertEquals(count(txGraph.getVertices()), 2);
        if (txGraph.getFeatures().supportsEdgeIteration)
            assertEquals(count(txGraph.getEdges()), 0);

        this.stopWatch();
        
        e = txGraph.addEdge(null, txGraph.getVertex(u.getId()), txGraph.getVertex(v.getId()), graphTest.convertLabel("test"));

        assertEquals(txGraph.getVertex(v.getId()), v);
        assertEquals(txGraph.getVertex(u.getId()), u);
        if (txGraph.getFeatures().supportsEdgeRetrieval)
            assertEquals(txGraph.getEdge(e.getId()), e);

        if (txGraph.getFeatures().supportsVertexIteration)
            assertEquals(count(txGraph.getVertices()), 2);
        if (txGraph.getFeatures().supportsEdgeIteration)
            assertEquals(count(txGraph.getEdges()), 1);
        assertEquals(e,getOnlyElement(txGraph.getVertex(u.getId()).getEdges(Direction.OUT)));
        txGraph.commit();
        printPerformance(txGraph.toString(), 1, "edge added in successful transaction (w/ iteration)", this.stopWatch());

        if (txGraph.getFeatures().supportsVertexIteration)
            assertEquals(count(txGraph.getVertices()), 2);
        if (txGraph.getFeatures().supportsEdgeIteration)
            assertEquals(count(txGraph.getEdges()), 1);

        assertEquals(txGraph.getVertex(v.getId()), v);
        assertEquals(txGraph.getVertex(u.getId()), u);
        if (txGraph.getFeatures().supportsEdgeRetrieval)
            assertEquals(txGraph.getEdge(e.getId()), e);
        assertEquals(e,getOnlyElement(txGraph.getVertex(u.getId()).getEdges(Direction.OUT)));

        txGraph.shutdown();
        
        graph.shutdown();
    }

    public void testBruteEdgeTransactions() {
        ThreadedTransactionalGraph graph = (ThreadedTransactionalGraph) graphTest.generateGraph();

        if (!graph.getFeatures().supportsThreadedTransactions) {
            return;
        }

        TransactionalGraph txGraph = graph.newTransaction();
        this.stopWatch();
        for (int i = 0; i < 100; i++) {
            Vertex v = txGraph.addVertex(null);
            Vertex u = txGraph.addVertex(null);
            txGraph.addEdge(null, v, u, graphTest.convertLabel("test"));
            txGraph.commit();
        }
        printPerformance(txGraph.toString(), 100, "edges added in 100 successful transactions (2 vertices added for each edge)", this.stopWatch());
        vertexCount(txGraph,200);
        edgeCount(txGraph,100);

        this.stopWatch();
        for (int i = 0; i < 100; i++) {
            Vertex v = txGraph.addVertex(null);
            Vertex u = txGraph.addVertex(null);
            txGraph.addEdge(null, v, u, graphTest.convertLabel("test"));
            txGraph.rollback();
        }
        printPerformance(txGraph.toString(), 100, "edges not added in 100 failed transactions (2 vertices added for each edge)", this.stopWatch());
        vertexCount(txGraph,200);
        edgeCount(txGraph,100);

        this.stopWatch();
        for (int i = 0; i < 100; i++) {
            Vertex v = txGraph.addVertex(null);
            Vertex u = txGraph.addVertex(null);
            txGraph.addEdge(null, v, u, graphTest.convertLabel("test"));
        }
        vertexCount(txGraph,400);
        edgeCount(txGraph,200);
        txGraph.commit();
        printPerformance(txGraph.toString(), 100, "edges added in 1 successful transactions (2 vertices added for each edge)", this.stopWatch());
        vertexCount(txGraph,400);
        edgeCount(txGraph,200);

        this.stopWatch();
        for (int i = 0; i < 100; i++) {
            Vertex v = txGraph.addVertex(null);
            Vertex u = txGraph.addVertex(null);
            txGraph.addEdge(null, v, u, graphTest.convertLabel("test"));
        }
        vertexCount(txGraph,600);
        edgeCount(txGraph,300);

        txGraph.rollback();
        printPerformance(txGraph.toString(), 100, "edges not added in 1 failed transactions (2 vertices added for each edge)", this.stopWatch());
        vertexCount(txGraph,400);
        edgeCount(txGraph,200);

        txGraph.shutdown();
        
        graph.shutdown();
    }

    public void testPropertyTransactions() {
        ThreadedTransactionalGraph graph = (ThreadedTransactionalGraph) graphTest.generateGraph();

        if (!graph.getFeatures().supportsThreadedTransactions) {
            return;
        }

        TransactionalGraph txGraph = graph.newTransaction();
        if (txGraph.getFeatures().supportsElementProperties()) {
            this.stopWatch();
            Vertex v = txGraph.addVertex(null);
            Object id = v.getId();
            v.setProperty("name", "marko");
            txGraph.commit();
            printPerformance(txGraph.toString(), 1, "vertex added with string property in a successful transaction", this.stopWatch());


            this.stopWatch();
            v = txGraph.getVertex(id);
            assertNotNull(v);
            assertEquals(v.getProperty("name"), "marko");
            v.setProperty("age", 30);
            assertEquals(v.getProperty("age"), 30);
            txGraph.rollback();
            printPerformance(txGraph.toString(), 1, "integer property not added in a failed transaction", this.stopWatch());

            this.stopWatch();
            v = txGraph.getVertex(id);
            assertNotNull(v);
            assertEquals(v.getProperty("name"), "marko");
            assertNull(v.getProperty("age"));
            printPerformance(txGraph.toString(), 2, "vertex properties checked in a successful transaction", this.stopWatch());

            Edge edge = txGraph.addEdge(null, v, txGraph.addVertex(null), "test");
            edgeCount(txGraph,1);
            txGraph.commit();
            edgeCount(txGraph,1);
            edge = getOnlyElement(txGraph.getVertex(v.getId()).getEdges(Direction.OUT));
            assertNotNull(edge);

            this.stopWatch();
            edge.setProperty("transaction-1", "success");
            assertEquals(edge.getProperty("transaction-1"), "success");
            txGraph.commit();
            printPerformance(txGraph.toString(), 1, "edge property added and checked in a successful transaction", this.stopWatch());
            edge = getOnlyElement(txGraph.getVertex(v.getId()).getEdges(Direction.OUT));
            assertEquals(edge.getProperty("transaction-1"), "success");

            this.stopWatch();
            edge.setProperty("transaction-2", "failure");
            assertEquals(edge.getProperty("transaction-1"), "success");
            assertEquals(edge.getProperty("transaction-2"), "failure");
            txGraph.stopTransaction(Conclusion.FAILURE);
            printPerformance(txGraph.toString(), 1, "edge property added and checked in a failed transaction", this.stopWatch());
            edge = getOnlyElement(txGraph.getVertex(v.getId()).getEdges(Direction.OUT));
            assertEquals(edge.getProperty("transaction-1"), "success");
            assertNull(edge.getProperty("transaction-2"));
        }
        txGraph.shutdown();
        
        graph.shutdown();
    }

    public void testIndexTransactions() {
        ThreadedTransactionalGraph graph = (ThreadedTransactionalGraph) graphTest.generateGraph();

        if (!graph.getFeatures().supportsThreadedTransactions) {
            return;
        }

        TransactionalGraph txGraph = graph.newTransaction();
        if (txGraph.getFeatures().supportsVertexIndex) {
            this.stopWatch();
            Index<Vertex> index = ((IndexableGraph) txGraph).createIndex("txIdx", Vertex.class);
            Vertex v = txGraph.addVertex(null);
            Object id = v.getId();
            v.setProperty("name", "marko");
            index.put("name", "marko", v);
            vertexCount(txGraph,1);
            v = getOnlyElement(((IndexableGraph) txGraph).getIndex("txIdx", Vertex.class).get("name", "marko"));
            assertEquals(v.getId(), id);
            assertEquals(v.getProperty("name"), "marko");
            txGraph.stopTransaction(Conclusion.SUCCESS);
            printPerformance(txGraph.toString(), 1, "vertex added and retrieved from index in a successful transaction", this.stopWatch());


            this.stopWatch();
            vertexCount(txGraph,1);
            v = getOnlyElement(((IndexableGraph) txGraph).getIndex("txIdx", Vertex.class).get("name", "marko"));
            assertEquals(v.getId(), id);
            assertEquals(v.getProperty("name"), "marko");
            printPerformance(txGraph.toString(), 1, "vertex retrieved from index outside successful transaction", this.stopWatch());


            this.stopWatch();
            v = txGraph.addVertex(null);
            v.setProperty("name", "pavel");
            index.put("name", "pavel", v);
            vertexCount(txGraph,2);
            v = getOnlyElement(((IndexableGraph) txGraph).getIndex("txIdx", Vertex.class).get("name", "marko"));
            assertEquals(v.getProperty("name"), "marko");
            v = getOnlyElement(((IndexableGraph) txGraph).getIndex("txIdx", Vertex.class).get("name", "pavel"));
            assertEquals(v.getProperty("name"), "pavel");
            txGraph.rollback();
            printPerformance(txGraph.toString(), 1, "vertex not added in a failed transaction", this.stopWatch());

            this.stopWatch();
            vertexCount(txGraph,1);
            assertEquals(count(((IndexableGraph) txGraph).getIndex("txIdx", Vertex.class).get("name", "pavel")), 0);
            printPerformance(txGraph.toString(), 1, "vertex not retrieved in a successful transaction", this.stopWatch());
            v = getOnlyElement(((IndexableGraph) txGraph).getIndex("txIdx", Vertex.class).get("name", "marko"));
            assertEquals(v.getProperty("name"), "marko");
        }
        txGraph.shutdown();
        
        graph.shutdown();
    }

    // public void testAutomaticIndexKeysRollback()

    public void testAutomaticSuccessfulTransactionOnShutdown() {
        ThreadedTransactionalGraph graph = (ThreadedTransactionalGraph) graphTest.generateGraph();

        if (!graph.getFeatures().supportsThreadedTransactions) {
            return;
        }

        TransactionalGraph txGraph = graph.newTransaction();
        if (txGraph.getFeatures().isPersistent && txGraph.getFeatures().supportsVertexProperties) {
            Vertex v = txGraph.addVertex(null);
            Object id = v.getId();
            v.setProperty("count", "1");
            v.setProperty("count", "2");
            txGraph.shutdown();
            txGraph = graph.newTransaction();
            Vertex reloadedV = txGraph.getVertex(id);
            assertEquals("2", reloadedV.getProperty("count"));

        }
        txGraph.shutdown();
        
        graph.shutdown();
    }

    public void testVertexCountOnPreTransactionCommit() {
        ThreadedTransactionalGraph graph = (ThreadedTransactionalGraph) graphTest.generateGraph();

        if (!graph.getFeatures().supportsThreadedTransactions) {
            return;
        }

        TransactionalGraph txGraph = graph.newTransaction();
        Vertex v1 = txGraph.addVertex(null);
        txGraph.commit();

        vertexCount(txGraph, 1);

        Vertex v2 = txGraph.addVertex(null);
        v1 = txGraph.getVertex(v1.getId());
        txGraph.addEdge(null, v1, v2, graphTest.convertLabel("friend"));

        vertexCount(txGraph, 2);

        txGraph.commit();

        vertexCount(txGraph, 2);
        txGraph.shutdown();
        
        graph.shutdown();
    }

    public void testBulkTransactionsOnEdges() {
        ThreadedTransactionalGraph graph = (ThreadedTransactionalGraph) graphTest.generateGraph();

        if (!graph.getFeatures().supportsThreadedTransactions) {
            return;
        }

        TransactionalGraph txGraph = graph.newTransaction();
        for (int i = 0; i < 5; i++) {
            txGraph.addEdge(null, txGraph.addVertex(null), txGraph.addVertex(null), graphTest.convertLabel("test"));
        }
        edgeCount(txGraph,5);
        txGraph.rollback();
        edgeCount(txGraph,0);

        for (int i = 0; i < 4; i++) {
            txGraph.addEdge(null, txGraph.addVertex(null), txGraph.addVertex(null), graphTest.convertLabel("test"));
        }
        edgeCount(txGraph,4);
        txGraph.stopTransaction(Conclusion.FAILURE);
        edgeCount(txGraph,0);


        for (int i = 0; i < 3; i++) {
            txGraph.addEdge(null, txGraph.addVertex(null), txGraph.addVertex(null), graphTest.convertLabel("test"));
        }
        edgeCount(txGraph,3);
        txGraph.stopTransaction(Conclusion.SUCCESS);
        edgeCount(txGraph,3);

        txGraph.shutdown();
        
        graph.shutdown();
    }


    public void testCompetingThreads() {
        final ThreadedTransactionalGraph graph = (ThreadedTransactionalGraph) graphTest.generateGraph();

        if (!graph.getFeatures().supportsThreadedTransactions) {
            return;
        }

        int totalThreads = 250;
        final AtomicInteger vertices = new AtomicInteger(0);
        final AtomicInteger edges = new AtomicInteger(0);
        final AtomicInteger completedThreads = new AtomicInteger(0);
        for (int i = 0; i < totalThreads; i++) {
            new Thread() {
                public void run() {
                    TransactionalGraph txGraph = graph.newTransaction();
                    Random random = new Random();
                    if (random.nextBoolean()) {
                        Vertex a = txGraph.addVertex(null);
                        Vertex b = txGraph.addVertex(null);
                        Edge e = txGraph.addEdge(null, a, b, graphTest.convertLabel("friend"));

                        if (txGraph.getFeatures().supportsElementProperties()) {
                            a.setProperty("test", this.getId());
                            b.setProperty("blah", random.nextFloat());
                            e.setProperty("bloop", random.nextInt());
                        }
                        vertices.getAndAdd(2);
                        edges.getAndAdd(1);
                        txGraph.commit();
                    } else {
                        Vertex a = txGraph.addVertex(null);
                        Vertex b = txGraph.addVertex(null);
                        Edge e = txGraph.addEdge(null, a, b, graphTest.convertLabel("friend"));
                        if (txGraph.getFeatures().supportsElementProperties()) {
                            a.setProperty("test", this.getId());
                            b.setProperty("blah", random.nextFloat());
                            e.setProperty("bloop", random.nextInt());
                        }
                        if (random.nextBoolean()) {
                            txGraph.commit();
                            vertices.getAndAdd(2);
                            edges.getAndAdd(1);
                        } else {
                            txGraph.rollback();
                        }
                    }
                    txGraph.shutdown();
                    completedThreads.getAndAdd(1);
                }
            }.start();
        }

        while (completedThreads.get() < totalThreads) {
        }
        assertEquals(completedThreads.get(), 250);
        edgeCount(graph,edges.get());
        vertexCount(graph,vertices.get());
        graph.shutdown();
    }
    
    public void testCompetingTransactions() {
        final ThreadedTransactionalGraph graph = (ThreadedTransactionalGraph) graphTest.generateGraph();

        if (!graph.getFeatures().supportsThreadedTransactions) {
            return;
        }

        Vertex testV = graph.addVertex(null);
        testV.setProperty("foo", "bar");
        graph.stopTransaction(Conclusion.SUCCESS);
        
        int totalTx = 25;
        TransactionalGraph[] txGraphs = new TransactionalGraph[totalTx];
        for (int i=0; i < totalTx; i++) {
            txGraphs[i] = graph.newTransaction();
        }
        
        for (int run=0; run < 3; run++) {
            // Add a bunch of edges in different transactions
            Edge[] testEdges = new Edge[totalTx];
            for (int i=0; i < totalTx; i++) {
                // Add a vertex
                Vertex outV = txGraphs[i].addVertex(null);
                outV.setProperty("foo", "bar");

                Vertex inV = txGraphs[i].addVertex(null);

                testEdges[i] = txGraphs[i].addEdge(null, outV, inV, "elabel");
            }

            // Make sure that only 3 vertexes are seen
            for (int i=0; i < totalTx; i++) {
                vertexCount(txGraphs[i], 3 + run * 2 * totalTx);
                edgeCount(txGraphs[i], 1 + run * totalTx);
            }

            // Commit
            for (int i=0; i < totalTx; i++) {
                txGraphs[i].commit();
            }

            // Everyone should see 1 + 2 * totalTx vertices and totalTx 
            for (int i=0; i < totalTx; i++) {
                vertexCount(txGraphs[i], 1 + 2 * totalTx * (1 + run));
                edgeCount(txGraphs[i], totalTx * (1 + run));
            }
        }

        for (int i=0; i < totalTx; i++) {
            txGraphs[i].shutdown();
        }

        graph.shutdown();
    }

//    public void testCompetingThreadsOnMultipleDbInstances() throws Exception {
//        // the idea behind this test is to simulate a rexster environment where two graphs of the same type
//        // are being mutated by multiple threads.  the test itself surfaced issues with OrientDB in such
//        // an environment and remains relevant for any graph that might be exposed through rexster.
//
//        final TransactionalGraph graph1 = (TransactionalGraph) graphTest.generateGraph("first");
//        final TransactionalGraph graph2 = (TransactionalGraph) graphTest.generateGraph("second");
//
//        if (!graph1.getFeatures().isRDFModel) {
//
//            final Thread threadModFirstGraph = new Thread() {
//                public void run() {
//                    final Vertex v = graph1.addVertex(null);
//                    v.setProperty("name", "stephen");
//                    graph1.commit();
//                }
//            };
//
//            threadModFirstGraph.run();
//            threadModFirstGraph.join();
//
//            final Thread threadReadBothGraphs = new Thread() {
//                public void run() {
//                    int counter = 0;
//                    for (Vertex v : graph1.getVertices()) {
//                        counter++;
//                    }
//
//                    Assert.assertEquals(1, counter);
//
//                    counter = 0;
//                    for (Vertex v : graph2.getVertices()) {
//                        counter++;
//                    }
//
//                    Assert.assertEquals(0, counter);
//                }
//            };
//
//            threadReadBothGraphs.run();
//            threadReadBothGraphs.join();
//        }
//
//        graph1.shutdown();
//        graph2.shutdown();
//    }

    public void testRemoveInTransaction() {
        ThreadedTransactionalGraph graph = (ThreadedTransactionalGraph) graphTest.generateGraph();

        if (!graph.getFeatures().supportsThreadedTransactions) {
            return;
        }

        TransactionalGraph txGraph = graph.newTransaction();
        edgeCount(txGraph,0);

        Vertex v1 = txGraph.addVertex(null);
        Object v1id = v1.getId();
        Vertex v2 = txGraph.addVertex(null);
        Edge e1 = txGraph.addEdge(null, v1, v2, graphTest.convertLabel("test-edge"));
        txGraph.commit();

        edgeCount(txGraph,1);
        e1 = getOnlyElement(txGraph.getVertex(v1id).getEdges(Direction.OUT));
        assertNotNull(e1);
        txGraph.removeEdge(e1);
        edgeCount(txGraph,0);
        assertNull(getOnlyElement(txGraph.getVertex(v1id).getEdges(Direction.OUT)));
        txGraph.rollback();

        edgeCount(txGraph,1);
        e1 = getOnlyElement(txGraph.getVertex(v1id).getEdges(Direction.OUT));
        assertNotNull(e1);

        txGraph.removeEdge(e1);
        txGraph.commit();

        edgeCount(txGraph,0);
        assertNull(getOnlyElement(txGraph.getVertex(v1id).getEdges(Direction.OUT)));
        txGraph.shutdown();
        
        graph.shutdown();
    }

}
