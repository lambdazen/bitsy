package com.lambdazen.bitsy.tx;

import java.util.HashMap;
import java.util.Map;

import com.lambdazen.bitsy.BitsyEdge;
import com.lambdazen.bitsy.BitsyException;
import com.lambdazen.bitsy.BitsyVertex;
import com.lambdazen.bitsy.IEdge;
import com.lambdazen.bitsy.IGraphStore;
import com.lambdazen.bitsy.UUID;
import com.lambdazen.bitsy.store.AdjacencyMap;
import com.lambdazen.bitsy.store.IEdgeRemover;

public class BitsyTransactionContext {
    Map<UUID, BitsyVertex> unmodifiedVertices;
    Map<UUID, BitsyEdge> unmodifiedEdges;
    Map<UUID, BitsyVertex> changedVertices;
    Map<UUID, BitsyEdge> changedEdges;
    IGraphStore store;
    AdjacencyMap adjMap;

    public BitsyTransactionContext(IGraphStore store) {
        this.unmodifiedVertices = new HashMap<UUID, BitsyVertex>();
        this.unmodifiedEdges = new HashMap<UUID, BitsyEdge>();
        this.changedVertices = new HashMap<UUID, BitsyVertex>();
        this.changedEdges = new HashMap<UUID, BitsyEdge>();
        this.store = store;

        this.adjMap = new AdjacencyMap(false, new IEdgeRemover() {
            @Override
            public IEdge removeEdge(UUID id) {
                return removeEdgeOnVertexDelete(id);
            }
        });
    }
    
    // This method is called to remove an edge through the IEdgeRemover
    private IEdge removeEdgeOnVertexDelete(UUID edgeId) throws BitsyException {
        // This is called from remove on adjMap, which means that the edge was added in this Tx
        BitsyEdge edge = changedEdges.remove(edgeId);
        
        // Only an edge that is present in this Tx can be removed by the IEdgeRemover
        assert (edge != null);
        
        return edge;
    }

    public void clear() {
        unmodifiedVertices.clear();
        unmodifiedEdges.clear();
        changedVertices.clear();
        changedEdges.clear();
        adjMap.clear();
    }
}
