package com.lambdazen.bitsy;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import com.lambdazen.bitsy.ads.dict.Dictionary;
import com.lambdazen.bitsy.ads.dict.Dictionary1;
import com.tinkerpop.blueprints.Element;

public abstract class BitsyElement implements Element {
    Object id;
    Dictionary properties;
    ITransaction tx;
    BitsyState state;
    int version;
    boolean updated;
    
    public BitsyElement(Object id, Dictionary properties, ITransaction tx, BitsyState state, int version) {
        this.id = id;
        this.properties = properties;
        this.tx = tx;
        this.state = state;
        this.version = version;
        this.updated = false;
    }
    
    public Object getId() {
        // No TX check to return the ID
        
        return id;
    }
    
    public Dictionary getPropertyDict() {
        return properties;
    }
    
    public <T>T getProperty(String key) {
        tx.validateForQuery(this);
        
        if (properties == null) {
            return null;
        } else {
            return (T) (properties.getProperty(key));
        }
    }

    public Set<String> getPropertyKeys() {
        tx.validateForQuery(this);
        
        if (properties == null) {
            return Collections.emptySet();
        } else {
            return new CopyOnWriteArraySet<String>(Arrays.asList(properties.getPropertyKeys()));
        }
    }
    
    public <T>T removeProperty(String key) {
        markForUpdate();
        
        if (properties == null) {
            return null;
        } else {
            Object ans = properties.getProperty(key);
            
            properties = properties.removeProperty(key);
    
            return (T)ans;
        }
    }

    public void setProperty(String key, Object value) {
        if (value == null) {
            throw new IllegalArgumentException("A null property can not be stored. You can call removeProperty() instead");
        }

        markForUpdate();
        
        if (key == null) {
            throw new IllegalArgumentException("Expecting non-null key in setProperty");
        } else if (key.length() == 0) {
            throw new IllegalArgumentException("Expecting non-empty key in setProperty");
        } else if (key.equals("id")) {
            throw new IllegalArgumentException("Can not set the 'id' property on an element");
        } else if (key.equals("label")) {
            throw new IllegalArgumentException("Can not set the 'label' property on an element");
        }
    
        if (this.properties == null) {
            this.properties = new Dictionary1(key, value);
        } else {
            this.properties = properties.setProperty(key, value);
        }

        assert (properties != null);
    }
    
    /** This method prepares the vertex/edge for an update */
    public void markForUpdate() {
        if (!updated) {
            updated = true;
            
            // Make a copy of the underlying property map, if non-null
            if (properties != null) {
                properties = properties.copyOf();
            }

            tx.markForPropertyUpdate(this);
        }
    }
    
    /** Added in Blueprints 2.3.0 to remove underlying vertex or edge */
    public abstract void remove();
    
    public BitsyState getState() {
        return state;
    }
    
    public void setState(BitsyState state) {
        this.state = state;
    }

    public int getVersion() {
        return version;
    }

    public ITransaction getTransaction() {
        return tx;
    }
    
    @Override
    public boolean equals(Object o) {
        if (o instanceof BitsyElement) {
            return ((BitsyElement)o).getId().equals(getId());
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}